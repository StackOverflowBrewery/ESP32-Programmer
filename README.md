# ESP32-Programmer

This is a simple programmer for my ESP32 based projects. It is based around the CH340B serial to USB converter
and features a slightly overdesigned 3.3V power supply as well as automatic programming for the ESP32. No need
to hold down and toggle buttons.

In this repository you will find an EasyEDA project export as well as BOM, Gerber and pick and place files exports.

![3D Rendering of the PCB](images/pcb-rendering.png)

## Features

* Modern USB-C connector
* Correct pull down resistors on CC1 and CC2
* Automatic reset and boot mode support when using esptool
* Rather beefy 3.3V power supply to support more peripherials

## Pinout

The programming connector is located on the bottom side of the PCB. so the pinout necessary on project boards is actually
mirrored. The pinout on this programmer board can be found in the EasyEDA project export in this repository. Here
is the pinout necessary on project boards:

```
         ________
3.3V ----| 1  2 |---- GND
TXD0 ----| 3  4 |---- RXD0
 IO0 ----| 5  6 |---- EN
         --------
```

## Current status

This is untested!
